package arrays;

public class RemoveElement {
    public static int removeDuplicates(int[] nums, int val) {
        int i = 0;
        for (final int num : nums) {
            if (num != val) {
                nums[i++] = num;
            }
        }
        return i;
    }

    public static void main(String[] args) {
        int[] nums = {0, 1, 2, 2, 3, 0, 4, 2};
        System.out.println("i: " + removeDuplicates(nums, 2));
    }
}
